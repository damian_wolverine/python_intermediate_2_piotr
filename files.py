def load_name(path):
    names = []

    with open(path, 'r') as f:
        for line in f:
            # print(line[:-1])
            line = line.strip()
            names.append(line)

    return names


def write_names(path, names):
    with open(path, 'w') as f:
        for name in names:
            f.write(f'{name}\n')


def append_names(path, names):
    with open(path, 'a') as f:
        # dołącza imiona na końcu pliku
        for name in names:
            f.write(f'{name}\n')


def sum_numbers_from_file(path):
    numbers = []
    with open(path, 'r') as f:
        for line in f:
            line = line.strip()
            numbers.append(int(line))

    return sum(numbers)


"""
dwa trybuty:
    - name
    - points
"""


class Subject:
    def __init__(self, name, points):
        self.name = name
        self.points = points


def load_subjects(path):
    subjects = []
    with open(path, 'r') as f:
        for line in f:
            line = line.strip()
            line = line.split(';')
            line[1] = int(line[1])
            subject = Subject(line[0], line[1])
            subjects.append(subject)
    # print(subjects)
    return subjects


def average(subjects):
    return sum([subject.points for subject in subjects]) / len(subjects)


def is_prime(subjects):
    results = []
    for subject in subjects:
        if subject.points == 2:
            results.append(subject)
        elif subject.points % 2:
            results2 = []
            for i in range(3, subject.points):
                if subject.points % i == 0:
                    results2.append(i)
            if len(results2) == 0:
                results.append(subject)
    return results


def letter_inside(subjects, letter='a'):
    # list_of_subjec_with_letter = [subject for subject in subjects if letter.lower() in subject.name.lower()]
    # dwie literki
    list_of_subjec_with_letter = [subject for subject in subjects if subject.name.count(letter) == 2]
    return list_of_subjec_with_letter


if __name__ == '__main__':
    # names_file = 'names.txt'
    # r = load_name(names_file)
    # print(r)
    #
    # write_names('names2.txt', ['Ewa', 'Piotr'])
    # append_names('names2.txt', ['Damian', 'Madzia'])
    # print(sum_numbers_from_file('numbers.txt'))
    # print(load_subjects('numbers.txt'))
    sub = (load_subjects('subjects.txt'))
    print(sub)
    for subject in sub:
        print (f'{subject.name} score {subject.points}')
    # print(average(sub))
    # is_prime_list = (is_prime(sub))
    # for subject in is_prime_list:
    #     print (subject.name, subject.points)
    # list_of_subjec_with_letter = letter_inside(sub, )
    # for subject in list_of_subjec_with_letter:
    #     print (subject.name, subject.points)

## Concatenate(SUBJECT):
# - Points -> ZWARACENE SĄ PRZEDMIOTY Z LICZBĄ PIERWSZĄ
# - Name -> ZWARACA PRZEDMIOTY GDZIE NAZWA ZAWIERA DWIE LITERY A
#
#
# def is_prime(subjects):
#     results = []
#     for subject in subjects:
#         if subject.points == 2:
#             results.append(subject)
#         elif subject.points % 2:
#             results2 = []
#             for i in range(3, subject.points):
#                 if subject.points % i == 0:
#                     results2.append(i)
#             if len(results2) == 0:
#                 results.append(subject)
#     return results
#
# # w mainie:
#     sub = (load_subjects('subjects.txt'))
#     is_prime_list = (is_prime(sub))
#     for subject in is_prime_list:
#         print (subject.name, subject.points)
#
# # do pliku txt dałem liczby którenie są liczbą pierwszą:
# Math;4
# Physics;7
# History;6
# Chemistry;5
# Geograpy;33