# Napisz funkcję która przyjmie listę przedmiotów a zwróci średnią wartość liczby punktów i wykorzystaj ją do swojego pliku z przedmiotami.
# utwórz: Klasa Person z atrybutami: name, age, height wyrażone w metrach jako liczba rzeczywista
# utwórz plik tekstowy persons.txt gdzie atrybuty konretnej osoby będą rozdzielone separatorem * (gwiazdki)
# napisz funkcję load_persons analogiczną do load subjects
# napisz funkcje sum_heights, która zwróci sumę wzrostów osób zapisanych w pliku testowym
# napisz funkcję save_persons która zapisze osoby do pliku tekstowego z separatorem: ==**==

class Person:
    def __init__(self, name, surname, age=30):
        self.name = name
        self.age = age
        self.surname = surname

    @property
    def signature(self):
        return f"My name is {self.name} and my surname is {self.age}"

    @signature.setter
    def signature(self, value):
        name, age = value.split()
        self.name = name
        self.age = age
        # print(name, surname)


if __name__ == '__main__':
    person1 = Person('Jan', 'Kowalski')
    person1.signature = 'Magda 25'
