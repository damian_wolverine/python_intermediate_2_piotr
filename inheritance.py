class Animal:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __str__(self):
        return f'My name is {self.name}, I am {self.age}'

    def __repr__(self):
        return f'Animal({self.name}, {self.age})'


class Dog(Animal):
    def __init__(self, name, age, tail_length):
        super().__init__(name, age)
        self.tail_length = tail_length

    def sound(self):
        return 'Raf Raf!'

    @staticmethod
    def run():
        return 'run'


class Husky(Dog):
    def __init__(self, name, age, tail_length, has_same_eyes):
        super().__init__(name, age, tail_length)
        self.has_same_eyes = has_same_eyes

    def sound(self):
        return f"{super().run()} {'Hau Hau'}"  # odwołanie do obiektu
        # Raf Raf! Hau Hau!
        # return f"{super().sound()} Hau Hau" # odwołanie do obiektu
        # return f"{Dog.run()}" # odwołanie do metody

    def __int__(self):
        return 17

    def __add__(self, other):
        return self.tail_length + other.tail_length

    def __len__(self):
        return self.tail_length


if __name__ == '__main__':
    animal1 = Animal('A', 24)
    print(animal1)
    print(repr(animal1))
    dog1 = Dog('Burek', 2, 12)
    print(dog1)
    print(dog1.sound())
    husky = Husky('Husky', 3, 11, True)
    husky2 = Husky('Husky2', 3, 20, True)
    print(husky.sound())
    print(husky.run())
    print(type(int("12")))
    print(int(husky))
    print(husky.__int__())
    print(husky + husky2)

    numbers = [1, 2, 3, 4]
    print(len(numbers))


# ## - na zajęciach kod od Piotrka part 1
#
# class Animal:
#     def __init__(self, name, age):
#         self.name = name
#         self.age = age
#
#     def __str__(self):
#         return f'My name is {self.name}, I am {self.age}'
#
#     def __repr__(self):
#         return f'Animal({self.name}, {self.age})'
#
#
# class Dog(Animal):
#     def __init__(self, name, age, tail_length):
#         super().__init__(name, age)
#         self.tail_length = tail_length
#
#     def sound(self):
#         return 'Raf Raf!'
#
#
# class Husky(Dog):
#     def __init__(self, name, age, tail_length, has_same_eyes):
#         super().__init__(name, age, tail_length)
#         self.has_same_eyes = has_same_eyes
#
#     def sound(self):
#         pass
#
#         # Raf Raf! Hau Hau!
#
#
# if __name__ == '__main__':
#     animal1 = Animal('A', 24)
#     print(animal1)
#     print(repr(animal1))
#     dog1 = Dog('Burek', 2, 12)
#     print(dog1)
#     print(dog1.sound())
#
# ## - na zajęciach kod od Piotrka part 2
#
# class Animal:
#     def __init__(self, name, age):
#         self.name = name
#         self.age = age
#
#     def __str__(self):
#         return f'My name is {self.name}, I am {self.age}'
#
#     def __repr__(self):
#         return f'Animal({self.name}, {self.age})'
#
#
# class Dog(Animal):
#     def __init__(self, name, age, tail_length):
#         super().__init__(name, age)
#         self.tail_length = tail_length
#
#     def sound(self):
#         return 'Raf Raf!'
#
#     @staticmethod
#     def run():
#         return 'run'
#
#
# class Husky(Dog):
#     def __init__(self, name, age, tail_length, has_same_eyes):
#         super().__init__(name, age, tail_length)
#         self.has_same_eyes = has_same_eyes
#
#     def sound(self):
#         return f"{super().run()} {'Hau Hau'}"
#         # Raf Raf! Hau Hau!
#
#     def __int__(self):
#         return 17
#
#     def __add__(self, other):
#         return self.tail_length + other.tail_length
#
#     def __len__(self):
#         return self.tail_length
#
#
# if __name__ == '__main__':
#     animal1 = Animal('A', 24)
#     print(animal1)
#     print(repr(animal1))
#     dog1 = Dog('Burek', 2, 12)
#     print(dog1)
#     print(dog1.sound())
#     husky = Husky('Husky', 3, 11, True)
#     husky2 = Husky('Husky2', 3, 20, True)
#     print(husky.sound())
#     print(husky.run())
#     print(type(int("12")))
#     print(int(husky))
#     print(husky.__int__())
#     print(husky + husky2)
#
#     numbers = [1, 2, 3, 4]
#     print(len(numbers))