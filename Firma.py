class Task:
    # w pythonie 3 nie musimy dawać nawiasów Task()
    def __init__(self, content, score, task_status=False):
        self.content = content
        self.score = score
        self.task_status = task_status

    def execute(self):
        self.task_status = True


class Employee:
    def __init__(self, name, age):
        self.name = name
        self.age = age
        self.task_list = []

    def description(self):
        return f"Hello, my name is {self.name} and I'm {self.age} old"

    def work_on(self):
        task_to_work = [task for task in self.task_list if task.task_status is False]
        task_to_work[0].execute()

    def add_task(self, task):
        self.task_list.append(task)

    @property
    def score(self):
        return sum([task.score for task in self.task_list if task.task_status is True])

    @property
    def monthly_salary(self):
        raise NotImplementedError()


class Salaried(Employee):
    def __init__(self, name, age, weekly_salary):
        super().__init__(name, age)
        self.weekly_salary = weekly_salary

    @property
    def monthly_salary(self):
        return self.weekly_salary * 4


class Hourly_worker(Employee):
    def __init__(self, name, age, count_of_hours, hourly_salary=30):
        super().__init__(name, age)
        self.count_of_hours = count_of_hours
        self.hourly_salary = hourly_salary

    @property
    def monthly_salary(self):
        return self.count_of_hours * self.hourly_salary * 4


class Firm:
    def __init__(self, name):
        self.name = name
        self.list_of_employees = []
        self.task_list_at_firm = []

    def add_employee(self, employee):
        self.list_of_employees.append(employee)

    def add_task(self, task):
        self.task_list_at_firm.append(task)

    def total_monthly_salary(self):
        return sum(emp_salary.monthly_salary for emp_salary in self.list_of_employees)

    def __len__(self):
        return len(self.list_of_employees)

    def welcome(self):
        return '\n'.join(f'Hello {employee.name} in my firm' for employee in self.list_of_employees)

if __name__ == "__main__":
    # task1 = Task("zadanie firma", 100)
    # print(task1.task_status)
    # task1.execute()
    # print(task1.task_status)
    # task2 = Task("zadanie firma 2", 20, task_status=True)
    # task3 = Task("zadanie firma 3", 50, task_status=True)
    # task4 = Task("zadanie firma 4", 70)
    # employee1 = Employee("Mietek", 40)
    # employee1.add_task(task2)
    # employee1.add_task(task1)
    # employee1.add_task(task3)
    # for task in employee1.task_list:
    #     print (task.content)
    # print(employee1.task_list[0].content)
    # print(employee1.score)
    emp_hourly = Hourly_worker('Stanisław', 40, 20)
    print(emp_hourly.monthly_salary)
    emp_monthly = Salaried('Czesław', 50, 1200)
    print(emp_monthly.monthly_salary)
    firma1 = Firm("Damian Master & Company")
    firma1.add_employee(emp_monthly)
    firma1.add_employee(emp_hourly)
    print(firma1.welcome())